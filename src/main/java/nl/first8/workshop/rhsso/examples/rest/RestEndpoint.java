package nl.first8.workshop.rhsso.examples.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;

import org.keycloak.services.resource.RealmResourceProvider;

public class RestEndpoint implements RealmResourceProvider {
    public static final String ID = "hello";

    @Override
    public Object getResource() {
        return this;
    }

    @GET
    @Produces("text/plain; charset=utf-8")
    public String get() {
        return "";
    }

    @Override
    public void close() {

    }
}
